from flask import Flask
from flask import json
import json
import csv

app = Flask(__name__)

@app.route("/api/", methods=["GET"])
def init_api():
    data = []
    try:
        with open("songs.csv", "r") as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                data.append(row)

            response = app.response_class(
                response = json.dumps(data, indent = 4),
                status=200,
                mimetype='application/json'
            )

    except IOError:
        print("I/O Error")

    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
