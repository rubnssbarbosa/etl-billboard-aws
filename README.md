# Billboard Data ETL and Upload to AWS S3

This repository consist of two modules:

1. An API with billboard top 100 music from 1956.
2. ETL - Extract data from API, Transform and Load to Amazon Web Services s3.

## Quick Usage

Make the api available

```bash
api/ $ python app.py
```
### Method GET
> GET http://localhost:5000/api/

## Features

* API with a get method that returns a JSON;
* Extract data from an API;
* Transform data;
* Load to Amazon S3.

## AWS bucket s3

As you can see the file was loaded successfully.

![aws](https://user-images.githubusercontent.com/17646546/99916147-2bde7980-2ce7-11eb-976f-3e627158ab66.png)
