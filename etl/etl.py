#!/usr/local/bin/python
import os
import sys
import json
import boto3
import logging
import requests
import pandas as pd
from config import client
from botocore.exceptions import ClientError

logging.basicConfig(stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

logger = logging.getLogger(__name__)


def extract():
    logger.info('EXTRACTING...')
    url = "http://localhost:5000/api/"
    req = requests.get(url)
    data = req.json()
    df = pd.DataFrame(data)
    return df

def transform(df_billboard):
    logger.info('TRANSFORMING...')
    logger.info('REMOVING COLUMNS')
    list_drop_columns = ['id', 'group', 'year']
    df_billboard.drop(list_drop_columns, axis=1, inplace=True)
    logger.info('CREATE DF .CSV')
    df_billboard.to_csv("data_billboard.csv", index=False)

def load_to_bucket_aws(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    logger.info('UPLOAD TO BUCKET')
    s3_client = client
    try:
        s3_client.upload_file(file_name, bucket, object_name)
        logger.info('UPLOAD SUCCESSFUL')
    except ClientError as e:
        logger.error("UPLOAD FAILED")
        logger.exception(e)
