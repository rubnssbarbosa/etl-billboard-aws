from etl import extract
from etl import transform
from etl import load_to_bucket_aws


if __name__ == "__main__":
    data = extract()
    transform(data)

    load_to_bucket_aws('data_billboard.csv', 'etl-billboard', None)
    
