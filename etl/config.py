#!/usr/local/bin/python
import os
import boto3


client = boto3.client(
    "s3",
    aws_access_key_id = os.environ.get("AWS_ACCESS_KEY"),
    aws_secret_access_key = os.environ.get("AWS_SECRET_KEY"),
    region_name = "us-east-2"
)
